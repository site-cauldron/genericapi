﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiteCauldron.GenericAPI.Database;

namespace SiteCauldron.GenericAPI.Controllers
{
    /// <summary>
    /// A controller to provide a full CRUD for any entity type in any given
    /// database context.
    /// </summary>
    /// <typeparam name="TContext">Context type in which the entities should be.</typeparam>
    /// <typeparam name="TEntity">Type of the entities.</typeparam>
    public abstract class AbstractController<TContext, TEntity> : ControllerBase
        where TContext : DbContext
        where TEntity : class
    {
        protected readonly TContext db;
        protected readonly DbSet<TEntity> set;

        /// <summary>
        /// Builds an AbstractController for the given entity in the given
        /// database context.
        /// </summary>
        /// <param name="context">Database context in which the entity is present.</param>
        /// <param name="set">Database set of the entity.</param>
        protected AbstractController(TContext context, DbSet<TEntity> dbset) =>
            (db, set) = (context, dbset);


        /// <summary>
        /// Retrieves an entity of the given id in the set or null if not
        /// found.
        /// </summary>
        /// <param name="id">Primary key or identificator of the entity.</param>
        /// <returns>The entity of the given id.</returns>
        [HttpGet("{id}")]
        public ActionResult<TEntity> Get(int id) => db.Find<TEntity>(id);

        /// <summary>
        /// Retrieves the full set of entities.
        /// </summary>
        /// <returns>The set of entities.</returns>
        [HttpGet]
        public ActionResult<DbSet<TEntity>> GetAll() => set;

        /// <summary>
        /// Adds an entity to the set, but does not commit it to the database.
        /// </summary>
        /// <remarks>
        /// In order to commit the transaction to the database, use
        /// <seealso cref="DbContext.SaveChanges"/> on the context object
        /// after calling this method, alternatively use <seealso cref="PutAndSave(TEntity)"/>
        /// to do it inmmediatly.
        /// </remarks>
        /// <param name="entity">Entity to add to the set.</param>
        /// <returns>The entity as it was added to the database.</returns>
        public ActionResult<TEntity> Put([FromBody] TEntity entity) => db.Add(entity).Entity;

        /// <summary>
        /// Modifies an entity of the set with the given one matching its id,
        /// or adds it if it does not exist yet, but does not commit it to the
        /// database.
        /// </summary>
        /// <remarks>
        /// In order to commit the transaction to the database, use
        /// <seealso cref="DbContext.SaveChanges"/> on the context object
        /// after calling this method, alternatively use <seealso cref="PatchAndSave(TEntity)"/>
        /// to do it inmmediatly.
        /// </remarks>
        /// <param name="entity">Entity to override in the set.</param>
        /// <returns>The entity as it was added to the database.</returns>
        public ActionResult<TEntity> Patch([FromBody] TEntity entity) => db.Update(entity).Entity;

        /// <summary>
        /// Removes an entity of the set with the given id, but does not commit
        /// it to the database.
        /// </summary>
        /// <remarks>
        /// In order to commit the transaction to the database, use
        /// <seealso cref="DbContext.SaveChanges"/> on the context object
        /// after calling this method, alternatively use <seealso cref="DeleteAndSave(int)"/>
        /// to do it inmmediatly.
        /// </remarks>
        /// <param name="id">Id of the entity to remove.</param>
        /// <returns>
        /// The entity as it used to be in the database, or null if
        /// it wasn't in the first place.
        /// </returns>
        public ActionResult<TEntity> Delete(int id) => db.Remove(db.Find<TEntity>(id)).Entity;

        /// <summary>
        /// Adds an entity to the set and commits it to the database.
        /// </summary>
        /// <param name="entity">Entity to add to the set.</param>
        /// <returns>The entity as it was added to the database.</returns>
        [HttpPut]
        public ActionResult<TEntity> PutAndSave([FromBody] TEntity entity) => db.SaveAfter(db.Add(entity));

        /// <summary>
        /// Modifies an entity of the set with the given one matching its id,
        /// or adds it if it does not exist yet and commits it to the
        /// database.
        /// </summary>
        /// <param name="entity">Entity to override in the set.</param>
        /// <returns>The entity as it was added to the database.</returns>
        [HttpPatch]
        public ActionResult<TEntity> PatchAndSave([FromBody] TEntity entity) => db.SaveAfter(db.Update(entity));

        /// <summary>
        /// Removes an entity of the set with the given id and commits
        /// it to the database.
        /// </summary>
        /// <param name="id">Id of the entity to remove.</param>
        /// <returns>
        /// The entity as it used to be in the database, or null if
        /// it wasn't in the first place.
        /// </returns>
        [HttpDelete("{id}")]
        public ActionResult<TEntity> DeleteAndSave(int id) => db.SaveAfter(db.Remove(db.Find<TEntity>(id)));
    }
}