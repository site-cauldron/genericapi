using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiteCauldron.GenericAPI.Database;

namespace SiteCauldron.GenericAPI.Controllers
{
    /// <summary>
    /// A controller to provide a full CRUD for any set of entities in any
    /// database context.
    /// </summary>
    /// <typeparam name="TContext">Database context to provide the CRUD.</typeparam>
    public class GenericController<TContext> : ControllerBase where TContext : DbContext
    {
        protected readonly TContext db;
        protected readonly GenericCRUD<TContext> gc;

        
        /// <summary>
        /// Builds a GenericController for the given database context and its
        /// given <see cref="GenericCRUD{TContext}"/>.
        /// </summary>
        /// <remarks>
        /// Use <seealso cref="GenericCRUD{TContext}"/> constructor to provide
        /// a CRUD for the desired database context.
        /// </remarks>
        /// <param name="context">Database context to provide the CRUD.</param>
        /// <param name="crud">Generic CRUD to expose in the controller endpoints.</param>
        public GenericController(TContext context, GenericCRUD<TContext> crud) =>
            (db, gc) = (context, crud);



        /// <summary>
        /// Wraps a given object under an <see cref="OkObjectResult"/>, but
        /// returns a <see cref="NotFoundObjectResult"/> if a null object
        /// is given.
        /// </summary>
        /// <param name="o">Object to wrap into the <see cref="OkObjectResult"/>.</param>
        /// <returns>
        /// An <see cref="ActionResult"/> representing the existance of the given ogject.
        /// </returns>
        public ActionResult AssertResponse(object o) =>
            o == null ? (ActionResult)NotFound() : Ok(o);

        
        
        /// <summary>
        /// Retrieves the hole entity set of the given entity type or a
        /// <see cref="NotFoundObjectResult"/> if no such set exists.
        /// </summary>
        /// <param name="entityType">Type of the entities to retrieve in plural.</param>
        /// <returns>The desired entity set.</returns>
        [HttpGet("{entityType}")]
        public ActionResult GetAll(string entityType) =>
            AssertResponse(gc.GetAll(db, entityType));


        /// <summary>
        /// Retrieves the specified entity of the given type and Id.
        /// </summary>
        /// <param name="entityType">Type of the entity to retrieve in plural.</param>
        /// <param name="id">Id of the entity to retrieve.</param>
        /// <returns>
        /// The entity or an <see cref="NotFoundObjectResult"/>
        /// if not such entity exists.
        /// </returns>
        [HttpGet("{entityType}/{id}")]
        public ActionResult Get(string entityType, int id) =>
            AssertResponse(gc.Get(db, entityType, id));


        /// <summary>
        /// Adds one or many entities of the given type to the database.
        /// </summary>
        /// <param name="entityType">Type of the entity to add in plural.</param>
        /// <param name="entity">Entity to add.</param>
        /// <returns>
        /// The entities added or <see cref="NotFoundObjectResult"/>
        /// if no such type exists.
        /// </returns>
        [HttpPut("{entityType}")]
        public ActionResult Put(string entityType, [FromBody] object entity) =>
            AssertResponse(gc.Put(db, entityType, entity));


        /// <summary>
        /// Updates or adds one or many entities to the database.
        /// </summary>
        /// <param name="entityType">Type of the entity to update in plural.</param>
        /// <param name="entity">Entity to update.</param>
        /// <returns>
        /// The entities updated or <see cref="NotFoundObjectResult"/>
        /// if no such type exists.
        /// </returns>
        [HttpPatch("{entityType}")]
        public ActionResult Patch(string entityType, [FromBody] object entity) =>
            AssertResponse(gc.Patch(db, entityType, entity));


        /// <summary>
        /// Removes an entity of the given type from the database.
        /// </summary>
        /// <param name="entityType">Type of the entity in plural.</param>
        /// <param name="id">Id of the entity to remove.</param>
        /// <returns>
        /// The removed entity or <see cref="NotFoundObjectResult"/>
        /// if no such entity exists.
        /// </returns>
        [HttpDelete("{entityType}/{id}")]
        public ActionResult Delete(string entityType, int id) =>
            AssertResponse(gc.DeleteOne(db, entityType, id));

        /// <summary>
        /// Removes many entities of the given type from the database.
        /// </summary>
        /// <param name="entityType">Type of the entity in plural.</param>
        /// <param name="ids">Ids of the entities to remove.</param>
        /// <returns>
        /// The removed entity or <see cref="NotFoundObjectResult"/>
        /// if no such entity exists.
        /// </returns>
        [HttpDelete("{entityType}")]
        public ActionResult Delete(string entityType, [FromBody] IEnumerable<int> ids) =>
            AssertResponse(gc.DeleteMany(db, entityType, ids));
    }
}
