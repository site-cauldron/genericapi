﻿using EntityGraphQL;
using EntityGraphQL.Schema;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace SiteCauldron.GenericAPI.Controllers
{
    /// <summary>
    /// GrapthQL controller for fetching information of any given database
    /// context.
    /// </summary>
    /// <typeparam name="TContext">Database context.</typeparam>
    public class GraphQLController<TContext> : ControllerBase where TContext : DbContext
    {
        protected readonly TContext db;
        protected readonly SchemaProvider<TContext> sp;

        /// <summary>
        /// Builds a GraphQLController given an instance of the database
        /// context and a <see cref="SchemaProvider{TContext}"/>
        /// </summary>
        /// <remarks>
        /// Use <seealso cref="SchemaBuilder.FromObject{TContext}(bool, bool)"/> to provide
        /// a <see cref="SchemaProvider{TContext}"/>.
        /// </remarks>
        /// <param name="context"></param>
        /// <param name="schemaProvider"></param>
        public GraphQLController(TContext context, SchemaProvider<TContext> schemaProvider) =>
            (db, sp) = (context, schemaProvider);

        /// <summary>
        /// Excecutes a GraphQL query on the database context.
        /// </summary>
        /// <param name="query">GraphQL query request.</param>
        /// <returns>A json string of the query result.</returns>
        [HttpPost]
        public ActionResult<string> Post([FromBody] QueryRequest query) =>
            Ok(JsonConvert.SerializeObject(sp.ExecuteQuery(query, db, null, null)));
    }
}