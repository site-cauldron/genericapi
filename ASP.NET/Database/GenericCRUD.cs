﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Pluralize.NET.Core;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SiteCauldron.GenericAPI.Database
{
    /// <summary>
    /// Provides a full CRUD for all of the entities of any desired database
    /// context.
    /// </summary>
    /// <typeparam name="TContext">Database context for which to provide the CRUD.</typeparam>
    public class GenericCRUD<TContext> where TContext : DbContext
    {
        /**** PROPERTIES ****/

        protected readonly Dictionary<string, Type> types;
        protected readonly Dictionary<string, PropertyInfo> sets;



        /**** CONSTRUCTORS ****/

        /// <summary>
        /// Builds a GenericCRUD for the desired database context.
        /// </summary>
        public GenericCRUD()
        {
            Pluralizer pluralizer = new Pluralizer();
            
            types = typeof(TContext)
                .GetProperties()
                .Where(p => p.PropertyType.Name
                    .StartsWith("DbSet", StringComparison.Ordinal))
                .Select(p => p.PropertyType.GenericTypeArguments.First())
                .ToDictionary(t => pluralizer
                    .Pluralize(t.Name)
                    .ToUpperInvariant());

            sets = typeof(TContext)
                .GetProperties()
                .Where(p => p.PropertyType.Name
                    .StartsWith("DbSet", StringComparison.Ordinal))
                .ToDictionary(p => p.Name
                    .ToUpperInvariant());
        }



        /**** HELPER FUNCTIONS ****/

        /// <summary>
        /// Retrieves the <see cref="Type"/> for the desired set in the
        /// database context.
        /// </summary>
        /// <param name="entityType">Type of the entity set in plural.</param>
        /// <returns>
        /// The <see cref="Type"/> of the entities or null if no such
        /// entity set exists.
        /// </returns>
        private Type EntityType(string entityType) =>
            types.GetValueOrDefault(entityType?.ToUpperInvariant());

        /// <summary>
        /// Retrieves the <see cref="PropertyInfo"/> of the desired entity
        /// set in the database context.
        /// </summary>
        /// <param name="entityType">Type of the entity set in plural.</param>
        /// <returns>
        /// The <see cref="PropertyInfo"/> of the entity set in the database
        /// context or null if no such entity set exists.
        /// </returns>
        private PropertyInfo EntitySet(string entityType) =>
            sets.GetValueOrDefault(entityType?.ToUpperInvariant());

        /// <summary>
        /// Calls the given method name on a database context with some
        /// object of the given type.
        /// </summary>
        /// <remarks>
        /// The transaction is not commited to the database after calling
        /// this method.
        /// </remarks>
        /// <param name="db">Database context from which to call the method.</param>
        /// <param name="entityType">Type of the entity in plural.</param>
        /// <param name="entity">Entity to provide in the method that will be called.</param>
        /// <param name="action">Mehtod name to call from the context.</param>
        /// <returns>The object after being used in the method.</returns>
        private EntityEntry MutateOne(TContext db, string entityType, JToken entity, string action)
        {
            Type type = EntityType(entityType);
            if(type == null) return null;

            return (EntityEntry)typeof(TContext)
                .GetGenericMethod(action, type)
                .Invoke(db, new object[] { entity.ToObject(type) }
            );
        }

        /// <summary>
        /// Calls the given method name on a database context with some
        /// object or lists of objects of the given type and commits the
        /// transaction to the database.
        /// </summary>
        /// <param name="db">Database context from which to call the method.</param>
        /// <param name="entityType">Type of the entity in plural.</param>
        /// <param name="entity">Entity or entities to provide in the method that will be called.</param>
        /// <param name="action">Mehtod name to call from the context.</param>
        /// <returns>The object after being used in the method.</returns>
        private object Mutate(TContext db, string entityType, object entity, string action) =>
            JToken.Parse(entity.ToString()) switch
            {
                JArray jArray => db.SaveRangeAfter(jArray.Select(jToken =>
                    MutateOne(db, entityType, jToken, action))),
                JToken jToken => db.SaveAfter(MutateOne(db, entityType, jToken, action))
            };



        /**** CRUD METHODS ****/

        /// <summary>
        /// Retrieves all of the entities of the specified entity set from the
        /// database.
        /// </summary>
        /// <param name="db">Database context from which to take the entities.</param>
        /// <param name="entityType">Type of the entities in plural.</param>
        /// <returns>The desired entities set.</returns>
        public object GetAll(TContext db, string entityType) =>
            EntitySet(entityType)?.GetValue(db);


        /// <summary>
        /// Retrieves an entity from the database of the given type and id.
        /// </summary>
        /// <param name="db">Database context from which to take the entity.</param>
        /// <param name="entityType">Type of the entity in plural.</param>
        /// <param name="id">Id of the entity.</param>
        /// <returns>The desired entity or null if no such entity exists.</returns>
        public object Get(TContext db, string entityType, int id)
        {
            Type t = EntityType(entityType);
            return t == null ? null : db?.Find(t, id);
        }


        /// <summary>
        /// Adds one or many entities of the given type to the database.
        /// </summary>
        /// <param name="db">Database context in which to add the entity.</param>
        /// <param name="entityType">Type of the entity in plural.</param>
        /// <param name="entity">Entity or entities to add in the database.</param>
        /// <returns>
        /// The same entity as it was added to the database or null if no such
        /// etity type exists.
        /// </returns>
        public object Put(TContext db, string entityType, object entity) =>
            Mutate(db, entityType, entity, "Add");


        /// <summary>
        /// Updates one or many entity in the database of the given type
        /// matching its or their Ids.
        /// </summary>
        /// <param name="db">Database context from which to update the entity.</param>
        /// <param name="entityType">Type of the entity in plural.</param>
        /// <param name="entity">Entity to update.</param>
        /// <returns>
        /// The same entity or entities as it was updated on the database or
        /// null if no such entity type exists.
        /// </returns>
        public object Patch(TContext db, string entityType, object entity) =>
            Mutate(db, entityType, entity, "Update");


        /// <summary>
        /// Removes an entity from the database of the given entity type.
        /// </summary>
        /// <param name="db">Database context from which to remove the entity</param>
        /// <param name="entityType">Type of the entity in plural.</param>
        /// <param name="id">Id of the entity to remove.</param>
        /// <returns>
        /// The removed entity or null if no such entity existed in the first
        /// place.
        /// </returns>
        public object DeleteOne(TContext db, string entityType, int id)
        {
            Type t = EntityType(entityType);
            if(t == null) return null;

            object entity = db?.Find(t, id);
            if(entity == null) return null;

            db.Entry(entity).State = EntityState.Detached;
            return Mutate(db, entityType, JsonConvert.SerializeObject(entity), "Remove");
        }

        /// <summary>
        /// Removes many entities from the database of the given entity type.
        /// </summary>
        /// <param name="db">Database context from which to remove the entity</param>
        /// <param name="entityType">Type of the entity in plural.</param>
        /// <param name="id">Ids of the entities to remove.</param>
        /// <returns>
        /// The removed entities or null if no such entity existed in the
        /// first place.
        /// </returns>
        public IEnumerable<object> DeleteMany(TContext db, string entityType, IEnumerable<int> ids) =>
            ids.ToHashSet().Select(id => DeleteOne(db, entityType, id));
    }
}