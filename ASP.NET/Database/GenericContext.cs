using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace SiteCauldron.GenericAPI.Database
{
    /// <summary>
    /// Extension methods for <see cref="DbContext"/>.
    /// </summary>
    public static class GenericContext
    {
        /// <summary>
        /// Recieves an EntityEntry, calls to <see cref="DbContext.SaveChanges"/>
        /// and returns just the Entity for the given EntityEntry.
        /// For example: db.SaveAfter(db.Remove(db.Find<T>(id))) will commit a
        /// deletion of an object of a model T with a given id in some given
        /// context db.
        /// </summary>
        /// <param name="dbContext">Database context in which to commit the transaction.</param>
        /// <typeparam name="T">Entity type.</typeparam>
        /// <param name="entity">Any entity entry from which to extract the entity.</param>
        /// <returns>The entity of the given entity entry.</returns>
        public static T SaveAfter<T>(this DbContext dbContext, EntityEntry<T> entity) where T : class
        {
            dbContext?.SaveChanges();
            return entity?.Entity;
        }

        /// <summary>
        /// Recieves an EntityEntry, calls to <see cref="DbContext.SaveChanges"/>
        /// and returns just the Entity for the given EntityEntry.
        /// For example: db.SaveAfter(db.Remove(db.Find<T>(id))) will commit a
        /// deletion of an object of a model T with a given id and in some given
        /// context db.
        /// </summary>
        /// <param name="dbContext">Database context in which to commit the transaction.</param>
        /// <param name="entity">Any entity entry from which to extract the entity.</param>
        /// <returns>The entity of the given entity entry.</returns>
        public static object SaveAfter(this DbContext dbContext, EntityEntry entity)
        {
            dbContext?.SaveChanges();
            return entity?.Entity;
        }

        /// <summary>
        /// Recieves an Enumerable of EntityEntry, extracts the Entities of
        /// every EntityEntry, calls to <see cref="DbContext.SaveChanges"/>
        /// and returns just the Entitries.
        /// </summary>
        /// <param name="dbContext">Database context in which to commit the transaction.</param>
        /// <param name="entities">Any entity entries from which to extract the entities.</param>
        /// <returns>The entities of the given entity entries.</returns>
        public static List<object> SaveRangeAfter(this DbContext dbContext, IEnumerable<EntityEntry> entities)
        {
            List<object> results = entities.Select(entity => entity.Entity).ToList();
            dbContext?.SaveChanges();
            return results;
        }

        /// <summary>
        /// Recieves an Enumerable of EntityEntry, extracts the Entities of
        /// every EntityEntry, calls to <see cref="DbContext.SaveChanges"/>
        /// and returns just the Entitries.
        /// </summary>
        /// <param name="dbContext">Database context in which to commit the transaction.</param>
        /// <param name="entities">Any entity entries from which to extract the entities.</param>
        /// <returns>The entities of the given entity entries.</returns>
        public static List<T> SaveRangeAfter<T>(this DbContext dbContext, IEnumerable<EntityEntry<T>> entities) where T : class
        {
            List<T> results = entities.Select(entity => entity.Entity).ToList();
            dbContext?.SaveChanges();
            return results;
        }
    }
}
