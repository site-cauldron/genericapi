using System;
using System.Linq;
using System.Reflection;

namespace SiteCauldron
{
    /// <summary>
    /// General purpose extensions for GenericAPI.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Retrieves the <see cref="MethodInfo"/> of a generic method given
        /// its type arguments.
        /// </summary>
        /// <param name="type">Type of which to obtain the generic method.</param>
        /// <param name="name">Name of the generic method.</param>
        /// <param name="typeArguments">Type arguments to fill in the generic method.</param>
        /// <returns>The <see cref="MethodInfo"/> for the generic method.</returns>
        public static MethodInfo GetGenericMethod(this Type type, string name, params Type[] typeArguments) =>
            type?.GetMethods().FirstOrDefault(m =>
                m.Name.Equals(name, StringComparison.Ordinal) && m.IsGenericMethod
            )?.MakeGenericMethod(typeArguments);
    }
}
