# SiteCauldron Generic API library for ASP.NET Core projects

This library contains several API controllers and tools to quickly create a
complete REST API for any given Entity Framework Core database context.

## Quick start
---


Add the package to your project:
```
dotnet add package SiteCauldron.GenericAPI
```

Create a database context:

```C#
public class SchoolContext : DbContext
{
    public SchoolContext(DbContextOptions<SchoolContext> options)
        : base(options) => Database.EnsureCreated();

    public DbSet<Student> Students { get; set; }
    public DbSet<Career> Careers { get; set; }
}

public class Student
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int CareerId { get; set; }
    public virtual Career Career { get; set; }
}

public class Career
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int MinPassingScore { get; set; }
    public virtual IEnumerable<Student> Students { get; set; }
}
```

You can create a complete CRUD for any given context and expose it in a
controller!

It's also possible to easily implement GraphQL for querying on the context thanks to [EntityGraphQL](https://github.com/lukemurray/EntityGraphQL), you can do both things in the services configuration on the server startup, and don't forget to configure your database context!

```C#
public void ConfigureServices(IServiceCollection services)
{
    services.AddControllers();

    // Configure the database context.
    services.AddDbContext<SchoolContext>(options => options
        .UseSqlite(Configuration.GetConnectionString("Default")));
    // Add GraphQL support.
    services.AddSingleton(SchemaBuilder.FromObject<SchoolContext>());
    // Add complete CRUD support.
    services.AddSingleton(new GenericCRUD<SchoolContext>());
}
```

You're almost done. Just implement both GraphQL and CRUD services on controllers.
```C#
[Route("api")]
[ApiController]
public class GeneralController : GenericController<SchoolContext>
{
    public GeneralController(
        SchoolContext context,
        GenericCRUD<SchoolContext> crud)
        : base(context, crud) { }
}

[Route("gql")]
[ApiController]
public class GraphQLImplementationController : GraphQLController<EjemploAlvContext>
{
    public GraphQLImplementationController(
        SchoolContext context,
        SchemaProvider<SchoolContext> schemaProvider)
        : base(context, schemaProvider) { }
}
```

And you're ready to go!



## Usage examples
---
### Adding
Say you have the previous context and you want to add John, a Math student, knowing the math career has an Id of 42, we could simply call the `/api/students` endpoint via `PUT` method with the following payload:
```json
{
    "name": "Jonh",
    "careerId": 42
}
```
The API will return the exact same payload with an extra field `Id` containing the id of the new entity added.

To add multiple entities, just put them in a Json list:
```json
[
    {
        "name": "Carl",
        "careerId": 6
    },
    {
        "name": "Peter",
        "careerId": 11
    }
]
```

### Reading
Say you want to see all of the students that are currently in the database to verify that John was added.

You could simply call the `/api/students` endpoint via `GET` method with no payload. You will get as a response a json with a list the information of all of the students.

Say you verified John was added with an `Id` of 12, you could obtain only John's info calling the `/api/students/12` endpoint via `GET` method with no payload.

### Updating
Say you realized you misspelled John and you want to fix it. Knowing that Jonh was added with the Id 12, you could call the `/api/students` endpoint via `PATCH` method with the following payload:
```json
{
    "id": 12,
    "name": "John",
    "careerId": 42
}
```

If the Id doesn't match with any existing entity so far, then it will be created a new one with the data given.

You can also update multiple entities, again, putting them in a list:
```json
[
    {
        "id": 20,
        "name": "Carl Gauss",
        "careerId": 6
    },
    {
        "id": 21,
        "name": "Peter Child",
        "careerId": 11
    }
]
```

### Deleting
Say John gave up on Math, so you don't need him on the database anymore. Knowing John's id is 12, all you have to do is to call the `/api/students/12` endpoint via `DELETE` method with no payload.

Delete multiple entities passing a list of Ids to the `api/students` endpoint via `DELETE` method. For example: `[1, 2, 5]`

## Querying with GraphQL
---
You can also make GraphQL queries using the `/gql` endpoint via `POST` method. Say for example you want to retrieve the students names ordered by career, and you want to quickly verify which career John is studying.

```
{
    careers {
        students { name }
    }
    johnsCareer: student(id: 12) {
        carrer { name }
    }
}
```

## Separate controllers by models
---
If you don't want to be so general, you can have a full CRUD functionality for each model separately, you just have to implement a controller for each model you want to expose:
```C#
[Route("[controller]")]
[ApiController]
public class StudentsController : AbstractController<SchoolContext, Student>
{
    public StudentsController(SchoolContext context)
        : base(context, context.Students) { }
}
```

By doing this you have the advantage to have a better control of the authorization for the models.

## Next steps
---

### Unleash the GraphQL power
GraphQL is a modern alternative to REST services, with it you can ask only for the data that you require, without having to fetch the hole entity,
you can also ask for many information in one single API call! Lear more at https://graphql.org/learn/queries/

### Authorize your controllers
Restrict the access to every model only to the authorized users. Controllers authorization is pretty much like it's normally done.

### Override the default methods
All API call methods are overridable!, so that you can implement a more complex logic on top of them, change the route or HTTP methods, or hide them.
